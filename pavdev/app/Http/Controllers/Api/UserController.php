<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use App\Attendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{

    public function register(Request $request){

        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'role_id' => 'required',
            'android_id' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['status'=>false,'error' => $validator->errors()], 401);
        }
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);
        
        $success['status'] = true;
        $success['message'] = 'Registration Successful';
        $success['token'] = $user->createToken('TeamPavDev')->accessToken;

        return response()->json($success, 200);
    }

    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user();

            $success['status'] =  true;
            $success['message'] =  'Login Successful';
            $success['android_id'] =  $user->android_id;
            $success['token'] =  $user->createToken('TeamPavDev')->accessToken;
            return response()->json($success, 200);
        } 
        else{ 
            return response()->json(['status' =>  false,'error'=>'Unauthorised'], 401); 
        } 
    }

    public function logout(){
        if(Auth::check()){
            Auth::user()->AauthAcessToken()->delete();
            return response()->json(['status'=> true, 'message'=>'Logout Sucessful'], 200);
        }
        else{
            return response()->json(['status'=> false,'message'=>'Unauthorised'], 401);
        }
    }
    
    public function profile(){
        
        $user = Auth::user();

        if($user){
            $user_data['id'] = $user->id;
            $user_data['name'] = $user->name;
            $user_data['email'] = $user->email;
            $user_data['timing'] = Attendance::where('user_id',$user->id)
                                            ->orderBy('id', 'desc')
                                            ->take(3)
                                            ->get();
            $data['status'] = true;
            $data ['message'] = 'User data found';
            $data['user'] = $user_data;
            return response()->json($data,200);
        }else{
            return response()->json(['status'=> false,'message'=>'Unauthorised'], 401);
        }
    }

    public function entry(){
        $user_id = Auth::id();

        $entry = new Attendance;
        $entry->user_id = $user_id;
        $entry->date = Carbon::now()->toDateString();
        $entry->entry_time = Carbon::now()->toTimeString();

        if($entry->save()){
            $data['status'] = true;
            $data['message'] = 'Entry timne added sucessfully';
            $data['entry'] = $entry;

            return response()->json($data,200);
        }
        else{
            return response()->json(['status'=> false,'message'=>'Entry Time add unsucessful'], 401);
        }
    }

    public function exit(){

        $user_id = Auth::id();

        $entry = Attendance::where('user_id',$user_id)
                            ->where('date', Carbon::now()->toDateString())
                            ->update(['exit_time' => Carbon::now()->toTimeString()]);

        if($entry){
            $today_timing = Attendance:: where('user_id',$user_id)
                                        ->where('date', Carbon::now()->toDateString())
                                        ->get();
            
            $data['status'] = true;
            $data['message'] = 'Exit timne added sucessfully';
            $data['today_timing'] = $today_timing;
            return response()->json($data,200);
        }else{
            return response()->json(['status'=> false,'message'=>'Timing not found'], 401);
        }
    }
}
