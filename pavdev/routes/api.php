<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::get('/home', function () {
//     return 'Home page from api';
//     //return view('welcome');
// });


Route::post('register','Api\UserController@register');
Route::post('login','Api\UserController@login');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('logout','Api\UserController@logout');
    Route::post('profile','Api\UserController@profile');
    Route::post('entry','Api\UserController@entry');
    Route::post('exit','Api\UserController@exit');
});