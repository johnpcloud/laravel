<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    protected $table = 'attendance_time';
    public $timestamps = false;

    //protected $guarded = ['user_id,entry_time,exit_time'];
}
